<?php
namespace App\Controllers;

use App\Resource\TrackResource;
use Slim\Views\Twig as View;


class TrackController extends Controller
{
    protected $trackResource;

    public function __construct($container, TrackResource $trackResource)
    {
        parent::__construct($container);
        $this->trackResource = $trackResource;
    }

    public function createForm($request, $response)
    {
        return $this->container->view->render($response, 'track.create.form.twig');
    }

    public function createNew($request, $response)
    {
        $name = $request->getParam('name');
        $youtubeId = $request->getParam('youtubeId');

        $this->trackResource->create($name,$youtubeId);

        return $this->container->view->render($response, 'track.create.success.twig');
    }

    public function get($request, $response)
    {
        $id = $request->getAttribute('id');
        $result = $this->trackResource->get($id);

        return json_encode($result);
    }
}