<?php
namespace App\Controllers;

use Slim\Views\Twig as View;
use App\Resource\TrackResource;

class HomeController extends Controller
{
    protected $trackResource;

    public function __construct($container, TrackResource $trackResource)
    {
        parent::__construct($container);
        $this->trackResource = $trackResource;
    }

    public function index($request, $response)
    {
        $results = $this->trackResource->getAll();

        return $this->container->view->render($response, 'home.twig', ['results' => $results]);

    }
}