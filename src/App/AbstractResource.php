<?php
namespace App;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use App\Entity\Track;

abstract class AbstractResource
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager = null;

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEntityManager()
    {
        if ($this->entityManager === null) {
            $this->entityManager = $this->createEntityManager();
        }

        return $this->entityManager;
    }

    /**
     * @return EntityManager
     */
    public function createEntityManager()
    {
        $path = array('Entity');
        $devMode = true;

        $config = Setup::createAnnotationMetadataConfiguration($path, $devMode);

        // define credentials...
        $connectionOptions = array(
            'driver'   => 'pdo_mysql',
            'host'     => '127.0.0.1',
            'dbname'   => 'dnd_soundboard',
            'user'     => 'root',
            'password' => '',
        );

        return EntityManager::create($connectionOptions, $config);
    }

    protected function toArray(Track $track) {
        return [
            'id' => $track->getId(),
            'name' => $track->getName(),
            'url' => $track->getUrl()
        ];
    }

    protected function toMultiArray($results){
        $toArray = [];
        foreach($results as $result){
            $toArray[$result->getID()]['name'] = $result->getName();
            $toArray[$result->getID()]['url'] = $result->getURL();
        }

        return $toArray;
    }
}