<?php

$app->get('/', 'HomeController:index');

$app->get('/track/create', 'TrackController:createForm');
$app->post('/track/create/new', 'TrackController:createNew');
$app->get('/track/get/{id}', 'TrackController:get');