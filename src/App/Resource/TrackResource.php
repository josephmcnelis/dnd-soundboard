<?php
namespace App\Resource;

use App\AbstractResource;
use App\Entity\Track;

/**
 * Class TrackResource
 * @package App
 */
class TrackResource extends AbstractResource
{

    public function create($name,$url)
    {
        /** @var Track $track */
        $track = new Track();
        $track->setName($name);
        $track->setUrl($url);

        $em = $this->getEntityManager();
        $em->persist($track);
        $em->flush();

        return 'Track '.$name.' Created Successfully!';
    }

    public function get($id)
    {
            $qb = $this->getEntityManager()->createQueryBuilder();
            $qb->select('t')
                ->from('App\Entity\Track', 't')
                ->where('t.id =  :id')
                ->setParameter('id', $id);
            $result = $qb->getQuery()->getOneOrNullResult();

            if(!is_null($result)){
                return $this->toArray($result);
            } else {
                return $result;
            }
    }

    public function getAll()
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('t')
            ->from('App\Entity\Track', 't');
        $results = $qb->getQuery()->getResult();
        $results = $this->toMultiArray($results);

        return $results;
    }
}

