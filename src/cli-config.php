<?php
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;

// replace with file to your own project bootstrap
require 'vendor/autoload.php';

$path = array('App/Entity');
$devMode = true;

// replace with mechanism to retrieve EntityManager in your app
$config = Setup::createAnnotationMetadataConfiguration($path, $devMode);

$connectionOptions = array(
    'driver'   => 'pdo_mysql',
    'host'     => 'localhost',
    'dbname'   => 'dnd_soundboard',
    'user'     => 'root',
    'password' => '',
);

$em = EntityManager::create($connectionOptions, $config);

return ConsoleRunner::createHelperSet($em);