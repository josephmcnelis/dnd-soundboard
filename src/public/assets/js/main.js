
$(document).ready(function(){

    $( ".play" ).click(function(e) {
        e.preventDefault();
        $.ajax({
            url: "/track/get/"+this.id,
            dataType:"json"
        }).done(function(result) {
            $('#modal').removeClass( "hidden" );
            var embed = '<iframe width="560" height="315" src="https://www.youtube.com/embed/'+result['url']+'?autoplay=1" frameborder="0" gesture="media" allowfullscreen></iframe>';
            $('#modal .modal-body').html( embed );
        });

    });

    $( ".close" ).click(function() {
        $('#modal .modal-body').html('');
        $('#modal').addClass( "hidden" );
    });
    
});
