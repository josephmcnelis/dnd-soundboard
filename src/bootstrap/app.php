<?php

require __DIR__.'/../vendor/autoload.php';

$configuration = [
    'settings' => [
        'displayErrorDetails' => true,
    ],
];
$container = new \Slim\Container($configuration);

$container['view'] = function ($container) {

    $view = new \Slim\Views\Twig(__DIR__ . '/../App/Views', [
        'cache' => false,
        'debug' => true
    ]);

    $view->addExtension(new \Slim\Views\TwigExtension(
        $container->router,
        $container->request->getUri()
    ));

    $view->addExtension(new \Twig_Extension_Debug());

    return $view;
};

$container['HomeController'] = function($container){
    return new \App\Controllers\HomeController($container,new \App\Resource\TrackResource());
};

$container['TrackController'] = function($container){
    return new \App\Controllers\TrackController($container,new \App\Resource\TrackResource());
};

$app = new \Slim\App($container);



require __DIR__ . '/../App/Routes/routes.php';